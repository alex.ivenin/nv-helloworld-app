job "run-proxy-1" {
  datacenters = ["dc1"]
  group "web" {
    count = 1

    task "proxy" {
      driver = "docker"

      resources {
        network {
          port "http" {
            static = 80
          }
        }
      }
      service {
        name = "proxy"
      }
      config {
	image = "registry.gitlab.com/alex.ivenin/nv-helloworld-app/nginx:0.0.11"
        dns_servers = ["127.0.0.1"]
      }
    }
  }
}


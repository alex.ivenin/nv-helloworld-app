job "run-hello-1" {
  datacenters = ["dc1"]
  group "web" {
    count = 1

    task "upstream" {
      driver = "docker"

      resources {
        network {
          port "http" {
            static = 8080
          }
	}
      }
      service {
        name = "app"
      }
      config {
        image = "registry.gitlab.com/alex.ivenin/nv-helloworld-app:0.0.1"
      }
    }
  }
}

